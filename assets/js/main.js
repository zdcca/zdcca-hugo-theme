const burger = document.getElementsByClassName('navbar-burger')[0];
const menu = document.getElementsByClassName('navbar-menu')[0];
burger.addEventListener('click', () => {
  burger.classList.toggle('is-active');
  menu.classList.toggle('is-active');
});

const navbar = document.getElementsByClassName('navbar')[0];
window.onscroll = function () {
  if (window.pageYOffset > 50) {
    navbar.classList.add('has-shadow');
  } else {
    navbar.classList.remove('has-shadow');
  }
}
